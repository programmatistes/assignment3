var colors = require('colors');
const mqtt = require('mqtt')
const client = mqtt.connect('mqtt://broker.hivemq.com')
var events = require('events');
const { count } = require('console');
var eventEmitter = new events.EventEmitter()
var intervalObject, current_temperature;

var state = 'off'
console.log(state.yellow)
client.on('connect', () => {
    client.subscribe('temperature/activation')
    client.publish('temperature/status', state)
    // set_interval()
    update_temperature()
})

function update_temperature() { 
    if(state == 'on') {
        current_temperature = Math.floor(Math.random() * (99 - 10) ) + 10;
        console.log(current_temperature.toString().yellow);
        client.publish('Temperature', current_temperature.toString());
    }
}
// eventEmitter.on('temp', update_temperature)

function set_interval() {
    intervalObject = setInterval( ()=> {
        // count = count + 1
        current_temperature = Math.floor(Math.random() * (99 - 10) ) + 10;
        client.publish('temeperature', current_temperature.toString());
        console.log("temp-->", current_temperature.toString())
        // eventEmitter.emit('temp')
    }, 1000); 
}

client.on('message', (topic, message) => {
    console.log((""+message).red)
    switch(topic) {
        case 'temperature/activation' :
            if(message == 'on') {
                turnOnRequest()
            } else if(message == 'off') {
                turnOffRequest()
            }
        break
        
        case 'temperature/status' :
            client.publish('temperature/status', state)
        break
        
        case 'Temperature' :
            update_temperature();
    }
})

function turnOnRequest() {
    if (state == 'on' || state == 'starting-up') return
    else {
        current_temperature = Math.floor(Math.random() * (99 - 10) ) + 10;
        state = 'on'
        console.log('(ON)'.yellow);
        client.publish('temperature/status', state)
        client.publish('Temperature', current_temperature.toString());
    //    setTimeout(() => {
    //            state = 'on'
    //            console.log('(ON)'.yellow);
    //            client.publish('temperature/status', state)
    //            client.publish('temperature', current_temperature.toString());
    //   }, 3000)
    }
}

function turnOffRequest() {
    if (state == 'off' || state == 'shutting-down') return
    else {
       state = 'off'
       console.log('(Shutting Down)'.yellow);
       client.publish('temperature/status', state)
       setTimeout(() => {
               state = 'off'
               console.log('(OFF)'.yellow);
               client.publish('temperature/status', state)
       }, 3000)
    }
}