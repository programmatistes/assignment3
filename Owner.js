var colors = require('colors');
const mqtt = require('mqtt')
const client = mqtt.connect('mqtt://broker.hivemq.com')

var state = 'home'
console.log(state.yellow)
client.on('connect', () => {
    client.publish('owner_status', state)
    detectOwnerMotion();
})

client.on('message', (topic, message) => {
    switch (topic) {
        case 'owner_status' :
            client.publish('owner_status', state)
    }
})

function detectOwnerMotion() {
    timeInterval = Math.floor(Math.random() * (10 - 1) ) + 1;
    setTimeout(publishOwnerStatus, timeInterval * 1000);
}

function publishOwnerStatus() {
    var myArray = [
        "home",
        "away"
      ];
    var randomItem = myArray[Math.floor(Math.random()*myArray.length)];
    state = randomItem
    console.log(randomItem)
    client.publish('owner', randomItem)
    detectOwnerMotion()
}