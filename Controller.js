var colors = require('colors');
var table = require("table");
const mqtt = require('mqtt')
const express = require('express');
const app = express();
const client = mqtt.connect('mqtt://broker.hivemq.com')

var state = '' 
var connected = false
var latestTemp, tempWarning = 'No warnings as of now.', ownerStatus, motionStatus = 'No motion detected.', lightStatus;
client.on('connect', () => {
    console.log('Temperature Sensor connected'.green);
    console.log('Owner Sensor connected'.green);
    console.log('light Sensor connected'.green);
    console.log('Motion Sensor connected'.green);
    
    client.subscribe('owner')
    client.subscribe('motion_detected')
    client.subscribe('Temperature')
    client.subscribe('temperature/status')
    client.subscribe('lights/status')
    console.log('sending request to turn on Temp')
    turnOnTemp()
})

function turnOnTemp () {
    if (state != 'on') {
    	client.publish('temperature/activation', 'on')
    }
}

function turnOffTemp () {
    if (state !== 'off') {
    	client.publish('temperature/activation', 'off')
    }
}

client.on('message', (topic, message) => {
    console.log("topic----->", topic)
    switch (topic) {
        case 'Temperature' :
            console.log("Current Temp is--->", message.toString())
            latestTemp = message.toString()
            if(message < 15) {
                tempWarning = 'Low Temp!'
                console.log("Low Temp Warning")
            } else if (message > 90) {
                tempWarning = 'High Temp!'
                console.log("High Temp Warning")
            }
        break

        case 'owner' :
            ownerStatus = message.toString()
            if(message == 'home') {
                console.log("Owner at Home, turning on the lights")
                client.publish('lights', 'on');
            } else if (message == 'away') {
                console.log("Owner not at Home, turning off the lights")
                client.publish('lights', 'off');
            }
        break
        
        case 'motion_detected' :
            motionStatus = message.toString()
            console.log(message.toString().red)
        break
        
        case 'lights/status' :
            console.log("light Status---->", message.toString())
            if(message.toString() == 'on') {
                lightStatus = "Lights are on!"
            } else {
                lightStatus = "Lights are off!!"
            }
            
            // lightStatus = message.toString()
        break    

    }
    if (topic == 'temperature/status') {
        state = message.toString()
        console.log(state)
      	console.log("Temperature status--->", message.toString())
    }
})

app.get('/', (req, res) => {
    var jsonData = {"TempValue" : latestTemp,
    "MotionStatus": motionStatus,
    "LightStatus" : lightStatus,
    "OwnerStatus" : ownerStatus,
    "TempWarning" : tempWarning};
    res.setHeader('Content-Type', 'application/json');
    res.send(jsonData)
});
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}...`);
});