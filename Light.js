var colors = require('colors');
const mqtt = require('mqtt')
const client = mqtt.connect('mqtt://broker.hivemq.com')

var state = 'off'
console.log(state.yellow)
client.on('connect', () => {
     client.subscribe('lights')
     client.publish('lights/status', state)
})

client.on('message', (topic, message) => {
    switch (topic) {
        case 'lights' :
            if(message == 'on') {
                if(state == 'off') {
                    console.log('Turning on the lights'.green)
                    client.publish('lights/status', state)
                    state = 'on'
                } else {
                    console.log('Already on'.red)
                    client.publish('lights/status', state)
                }
            } else {
                if(state == 'on') {
                    console.log('Turning off the lights'.yellow)
                    client.publish('lights/status', state)
                    state = 'off'
                } else {
                    console.log('Already off'.red)
                    client.publish('lights/status', state)
                }
            }
        break
        
        case 'lights/status' :
            client.publish('lights/status', state)
        break    
    }
})